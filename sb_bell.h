#ifndef _SB_BELL_H_
#define _SB_BELL_H_

#include "webhook.h"
#include "backand.h"
#include "push_button.h"
#include "buzzer.h"
#include "led.h"
#include "sb_data.h"

class SbBell {
    public:
        // Data Model
        SbData      data;

        // Hardware
        PushButton  *comingButton;
        PushButton  *comeUpButton;
        PushButton  *awayButton;
        Led         *awayLed;
        Buzzer      *buzzer;

        SbBell(int comingButtonPin, int comeUpButtonPin, int awayButtonPin, int awayLedPin, int buzzerPin);
        void    init(void);
        void    loop(void);
        void    setAwayLed(void);

        int     eventDispatcher(String event);
        void    displayEventDispatcher(const char *particleEvent, const char *sbEvent);
        int     dbEventDispatcher(String event);
        void    buzzerTimerCallBack(void);
        void    getBellData(const char *event, const char *dataString);
        void    triggerDataUpdate(void);
};

#endif
