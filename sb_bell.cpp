#include "sb_bell.h"

SbBell::SbBell(int comingButtonPin, int comeUpButtonPin, int awayButtonPin, int awayLedPin, int buzzerPin) {
    comingButton = new PushButton(comingButtonPin);
    comeUpButton = new PushButton(comeUpButtonPin);
    awayButton = new PushButton(awayButtonPin);
    awayLed = new Led(awayLedPin);
    buzzer = new Buzzer(buzzerPin);
}

void SbBell::init(void) {
    comingButton->init();
    comeUpButton->init();
    awayButton->init();
    awayLed->init();
    buzzer->init(&SbBell::buzzerTimerCallBack, *this);

    Particle.subscribe("from_display", &SbBell::displayEventDispatcher, this, MY_DEVICES);
    Webhook::subscribe("get_bell_data", &SbBell::getBellData, this);
    Particle.function("from_db", &SbBell::dbEventDispatcher, this);

    triggerDataUpdate();
}

void SbBell::loop(void) {
    if (comingButton->checkPressed()) {
        buzzer->stopBuzz();
        Webhook::publish("answer_button_pressed", {{"backand_anonymous_token", BACKAND_ANONYMOUS_TOKEN}, {"bell_id", data.id}, {"button", "coming"}});
    }

    if (comeUpButton->checkPressed()) {
        buzzer->stopBuzz();
        Webhook::publish("answer_button_pressed", {{"backand_anonymous_token", BACKAND_ANONYMOUS_TOKEN}, {"bell_id", data.id}, {"button", "come_up"}});
    }

    if (awayButton->checkPressed()) {
        data.away = !data.away;
        setAwayLed();
        Webhook::publish("away_button_pressed", {{"backand_anonymous_token", BACKAND_ANONYMOUS_TOKEN}, {"bell_id", data.id}, {"away", data.away ? "true" : "false"}, {"sender", "bell"}});
    }
}

int SbBell::eventDispatcher(String event) {
    if (event.equals("update_data")) {
        triggerDataUpdate();
    }

    if (event.equals("ring")) {
        Particle.publish("from_bell", "print_ringing_msg", 60, PRIVATE);
        buzzer->startBuzz(440, BUZZ_DURATION);
    }

    if (event.equals("set_away_true")) {
        data.away = true;
        setAwayLed();
    }

    if (event.equals("set_away_false")) {
        data.away = false;
        setAwayLed();
    }
}

void SbBell::displayEventDispatcher(const char *particleEvent, const char *sbEvent) {
    String event = String(sbEvent);
    eventDispatcher(event);
}

int SbBell::dbEventDispatcher(String event) {
    eventDispatcher(event);
}

void SbBell::buzzerTimerCallBack(void) {
    // No one answered the door after the buzzer ended so we call a CA that:
    // - sends a push notification
    // - sends a print no answer message to the display
}

void SbBell::triggerDataUpdate(void) {
    Webhook::publish("get_bell_data", {{"backand_anonymous_token", BACKAND_ANONYMOUS_TOKEN}, {"device_type", "particle_bell_id"}, {"device_id", System.deviceID()}});
}

void SbBell::getBellData(const char *event, const char *dataString) {
    data.populate(dataString);
    setAwayLed();
}

void SbBell::setAwayLed(void) {
    (data.away == true) ? awayLed->on() : awayLed->off();
}
