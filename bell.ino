/********/
/* BELL */
/********/

#include "sb_bell.h"

SbBell  bell(D2, D1, D3, D4, A4);

void setup() {
    Serial.begin(9600);
    bell.init();
}

void loop() {
    bell.loop();
}
